import "./App.css";
import Confirmation from "./components/Confirmation";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={"/confirmation"} element={<Confirmation />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
